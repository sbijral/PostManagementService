package com.yolo.PostManagementService.resources;

import java.util.List;

import javax.annotation.Generated;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.yolo.PostManagementService.enums.PostStatus;
import com.yolo.PostManagementService.enums.PostType;
@Getter
@Setter
@Document(collection = "Post")
public class Post {




	@Id
	private String postId;
	private String location;
	private String title;
	private String description;
	private String postedBy;
	private String category;
	private boolean isPromoted;
	private PostType type;
	private Long createdTime;
	private Long lastUpdatedTime;
	private List<String> spamReporters;
	private List<String> photoLinks;
	private Integer likeCounts;
	private Integer commentsCounts;
	private List<String> likerIds;
	private String posterName;
	private String posterProfilePictureLink;
	private List<String> postFollowers;

	private PostStatus status;

	public Post(String postId2) {
		// TODO Auto-generated constructor stub
		this.postId=postId2;
	}

	public Post() {
		// TODO Auto-generated constructor stub
	}


}
